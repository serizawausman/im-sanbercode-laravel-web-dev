<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("Shaun");
echo "name: " . $sheep->get_name() . "<br>";
echo "legs: " . $sheep->get_legs() . "<br>";
echo "cold blooded: " . $sheep->get_cold_blooded() . "<br><br>";

$kodok = new Frog("Shaun");
echo "name: " . $kodok->get_name() . "<br>";
echo "legs: " . $kodok->get_legs() . "<br>";
echo "cold blooded: " . $kodok->get_cold_blooded() . "<br>";
echo "jump: " . $kodok->jump() . "<br><br>";

$sungokong = new Ape("Kera sakti");
echo "name: " . $sungokong->get_name() . "<br>";
echo "legs: " . $sungokong->get_legs() . "<br>";
echo "cold blooded: " . $sungokong->get_cold_blooded() . "<br>";
echo "yell: " . $sungokong->yell() . "<br>";
