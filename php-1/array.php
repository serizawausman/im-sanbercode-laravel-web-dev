<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php

    echo "<h3> Soal 1 </h3>";
    /* 
        SOAL NO 1
        Kelompokkan nama-nama di bawah ini ke dalam Array.
        Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
        Adults: "Hopper", "Nancy", "Joyce", "Jonathan", "Murray"
    */
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    $adults = ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"];

    echo "<h4>Kids:</h4>";
    echo "<ul>";
    foreach ($kids as $kid) {
        echo "<li>$kid</li>";
    }
    echo "</ul>";

    echo "<h4>Adults:</h4>";
    echo "<ul>";
    foreach ($adults as $adult) {
        echo "<li>$adult</li>";
    }
    echo "</ul>";

    echo "<h3> Soal 2</h3>";
    /* 
        SOAL NO 2
        Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
    */
    echo "<h3> Soal 1 </h3>";
    /* 
    SOAL NO 1
    Kelompokkan nama-nama di bawah ini ke dalam Array.
    Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
    Adults: "Hopper", "Nancy", "Joyce", "Jonathan", "Murray"
    */
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    $adults = ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"];

    echo "<h4>Kids:</h4>";
    echo "<ul>";
    foreach ($kids as $kid) {
        echo "<li>$kid</li>";
    }
    echo "</ul>";

    echo "<h4>Adults:</h4>";
    echo "<ul>";
    foreach ($adults as $adult) {
        echo "<li>$adult</li>";
    }
    echo "</ul>";

    echo "<h3> Soal 2 </h3>";
    /*
    SOAL NO 2
    Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
*/
    echo "<h4>Kids Array Length: " . count($kids) . "</h4>";
    echo "<h4>Kids Array Content:</h4>";
    echo "<ul>";
    foreach ($kids as $kid) {
        echo "<li>$kid</li>";
    }
    echo "</ul>";

    echo "<h4>Adults Array Length: " . count($adults) . "</h4>";
    echo "<h4>Adults Array Content:</h4>";
    echo "<ul>";
    foreach ($adults as $adult) {
        echo "<li>$adult</li>";
    }
    echo "</ul>";
    /* 
        SOAL No 3
        Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
    */
    $characters = [
        [
            "Name" => "Will Byers",
            "Age" => 12,
            "Aliases" => "Will the Wise",
            "Status" => "Alive"
        ],
        [
            "Name" => "Mike Wheeler",
            "Age" => 12,
            "Aliases" => "Dungeon Master",
            "Status" => "Alive"
        ],
        [
            "Name" => "Jim Hopper",
            "Age" => 43,
            "Aliases" => "Chief Hopper",
            "Status" => "Deceased"
        ],
        [
            "Name" => "Eleven",
            "Age" => 12,
            "Aliases" => "El",
            "Status" => "Alive"
        ]
    ];

    echo "<pre>";
    print_r($characters);
    echo "</pre>";
    ?>
</body>

</html>