<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ulasan extends Model
{
    use HasFactory;
    protected $table = 'ulasan';
    protected $fillable = ['ulasan', 'rating', 'tanggal', 'user_id', 'film_id'];

    public function user() {
        return $this->belongsTo(user::class, 'user_id');
    }
    public function film() {
        return $this->belongsTo(film::class, 'film_id');
    }
}
