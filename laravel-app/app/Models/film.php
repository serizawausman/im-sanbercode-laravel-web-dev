<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    use HasFactory;
    protected $table = 'film';
    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poter', 'genre_id'];

    public function genre() {
        return $this->belongsTo(genre::class, 'genre_id');
    }

    public function ulasan() {
        return $this->hasMany(ulasan::class, 'film_id');
    }
}
