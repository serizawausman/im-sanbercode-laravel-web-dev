<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\ulasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UlasanController extends Controller
{
    public function tambah(Request $request, $id){
        $request->validate([
            'ulasan' => 'required',
            'rating' => 'required|integer|min:1|max:5'
        ]);

        $iduser = Auth::id();
        
        $ulasan = new ulasan;
        $ulasan->ulasan = $request->ulasan;
        $ulasan->rating = $request->rating;
        $ulasan->tanggal = Carbon::now();
        $ulasan->user_id = $iduser;
        $ulasan->film_id = $id;

        $ulasan->save();

        return redirect('/film/' . $id);
    }
}
