<?php

namespace App\Http\Controllers;

use App\Models\genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        DB::table('genre')->insert([
            'nama' => $request->input('nama'),
        ]);

        return redirect('/genre');
    }
    public function index()
    {

        $genre = DB::table('genre')->get();
        return view('genre.index', ['genre' => $genre]);
    }
    public function show($id)
    {
        $genre = genre::find($id);
        return view('genre.detail',['genre' => $genre]);
    }
    public function edit($id)
    {
        $genre = DB::table('genre')->where('id', $id)->first();
        return view('genre.edit', ['genre' => $genre]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);
        DB::table('genre')->where('id', $id)
            ->update([
                'nama' => $request->nama
            ]);
        return redirect('/genre');
    }
    public function delete($id)
    {

        DB::table('genre')->where('id', $id)->delete();
        return redirect('/genre');
    }
}
