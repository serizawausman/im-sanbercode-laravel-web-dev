<?php

namespace App\Http\Controllers;

use App\Models\profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $iduser = Auth::id();
        $detailprofile = profile::where('user_id', $iduser)->first();
        return view('profile.index', ['detailprofile' => $detailprofile]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profile = profile::find($id);
        
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;
        $profile->save();

       return redirect('/profile');
        
    }
}
