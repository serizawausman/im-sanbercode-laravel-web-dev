<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UlasanController;
use Symfony\Component\HttpKernel\Profiler\Profile;

Route::get('/', [HomeController::class, 'index']);
Route::get('/table', [HomeController::class, 'table']);
Route::get('/data-table', [HomeController::class, 'data']);

Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'delete']);

Route::middleware('auth')->group(function(){
    Route::get('/genre/create', [GenreController::class, 'create']);
    Route::post('/genre', [GenreController::class, 'store']);
    Route::get('/genre', [GenreController::class, 'index']);
    Route::get('/genre/{genre_id}', [GenreController::class, 'show']);
    Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);
    Route::put('/genre/{genre_id}', [GenreController::class, 'update']);
    Route::delete('/genre/{genre_id}', [GenreController::class, 'delete']);

    Route::resource('profile', ProfileController::class)->only(['index', 'update']);
    Route::post('/komentar/{film_id}', [UlasanController::class, 'tambah']);
});

Route::resource('/film', FilmController::class);
Auth::routes();
