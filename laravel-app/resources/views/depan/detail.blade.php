@extends('layout.master')
@section('judul')
    <h1>Halaman Detail Cast</h1>
@endsection
@section('content')
<p>{{ $cast->nama }}</p>
<p>{{ $cast->umur }}</p>
<p>{{ $cast->bio }}</p>
<a href="/cast" class="btn btn-primary">Kembali</a>
@endsection
