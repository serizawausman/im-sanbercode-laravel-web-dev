@extends('layout.master')
@section('judul')
    <h1>Halaman Edit Cast</h1>
@endsection
@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" value="{{ $cast->nama }}" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="date" class="form-control" value="{{ $cast->umur }}" name="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Descripsi</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{ $cast->bio }}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
