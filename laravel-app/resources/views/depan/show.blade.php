@extends('layout.master')
@section('judul')
    <h1>Halaman List Cast</h1>
@endsection
@section('content')
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Descripsi</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($cast as $key => $value)
         <tr>
            <td>{{ $key +1 }}</td>
            <td>{{ $value->nama }}</td>
            <td>{{ $value->umur }}</td>
            <td>{{ $value->bio }}</td>
            <td>
                <form action="/cast/{{ $value->id }}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{ $value->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{ $value->id }}/edit" class="btn btn-warning btn-sm">Ubah</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
         </tr>
     @empty
         <tr>
            <td>Tidak ada data</td>
         </tr>
     @endforelse
    </tbody>
  </table>
<a href="/cast/create" class="btn btn-primary">Tambah Cast</a>
@endsection
