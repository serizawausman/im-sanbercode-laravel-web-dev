@extends('layout.master')
@section('judul')
    <h1>Update Profile</h1>
@endsection
@section('content')
<form action="/profile/{{ $detailprofile->id }}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label>User</label>
        <input type="text" value="{{ $detailprofile->user->name }}" class="form-control" disabled>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" value="{{ $detailprofile->user->email }}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>umur</label>
        <input type="number" value="{{ $detailprofile->umur }}" class="form-control" name="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{ $detailprofile->bio }}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10">{{ $detailprofile->alamat }}</textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
