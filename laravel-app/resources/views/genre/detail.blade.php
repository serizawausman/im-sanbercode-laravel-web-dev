@extends('layout.master')
@section('judul')
    <h1>Halaman Detail Genre</h1>
@endsection
@section('content')
<p>{{ $genre->nama }}</p>
<div class="row">
@forelse ($genre->film as $item)
<div class="card">
    <img src="{{ asset('image/' . $item->poster ) }}" class="card-img-top img-medium" alt="Poster of {{ $item->judul }}">
    <div class="card-body">
        <h5>{{ $item->judul }}</h5>
        <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
        <p class="card-text"><strong>Tahun:</strong> {{ $item->tahun }}</p>
        <a href="/film/{{ $item->id }}" class="btn btn-secondary btn-block btn-sm">Detail</a>
    </div>
</div>
@empty
    <h3>Tidak ada postingan</h3>
@endforelse
</div>
<a href="/genre" class="btn btn-primary">Kembali</a>
@endsection
