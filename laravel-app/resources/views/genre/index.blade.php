@extends('layout.master')

@section('judul')
    <h1>Halaman List Genre</h1>
@endsection

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key => $value)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $value->nama }}</td>
                    <td>
                        <form action="/genre/{{ $value->id }}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/genre/{{ $value->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/genre/{{ $value->id }}/edit" class="btn btn-warning btn-sm">Ubah</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    <a href="/genre/create" class="btn btn-primary">Tambah Genre</a>
@endsection
