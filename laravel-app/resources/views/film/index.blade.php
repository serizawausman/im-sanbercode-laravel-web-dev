@extends('layout.master')

@section('judul')
    <h1>Halaman Film</h1>
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary mb-3">Tambah Film</a>
@endauth
<div class="row">
    @forelse ($film as $item)
    <div class="col-md-4 mb-4">
        <div class="card">
            <img src="{{ asset('image/' . $item->poster ) }}" class="card-img-top img-medium" alt="Poster of {{ $item->judul }}">
            <div class="card-body">
                <h5>{{ $item->judul }}</h5>
                <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
                <p class="card-text"><strong>Tahun:</strong> {{ $item->tahun }}</p>
                <a href="/film/{{ $item->id }}" class="btn btn-secondary btn-block btn-sm">Detail</a>
                <span class="badge badge-info">{{ $item->genre->nama }}</span>
                @auth
                <div class="row my-2">
                    <div class="col">
                        <a href="/film/{{ $item->id }}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
                    </div>
                    <div class="col">
                        <form action="/film/{{ $item->id }}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-block btn-sm" value="delete">
                        </form>
                    </div>
                </div>
                @endauth
            </div>
        </div>
    </div>
    @empty
        <h2>Tidak ada Film</h2>
    @endforelse
</div>
@endsection
