@extends('layout.master')
@section('judul')
    <h1>Halaman Tambah Film</h1>
@endsection
@section('content')
<form action="/film/{{ $film->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" value="{{ $film->judul }}" name="judul">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{ $film->ringkasan }}</textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tahun</label>
        <input type="date" class="form-control" value="{{ $film->tahun }}" name="tahun">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control" name="poster">
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">--Pilih--</option>
            @forelse ($genre as $item)
            @if ($item->id === $film->genre_id)
            <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
            @else
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endif
                
            @empty
                <option value="">Tidak ada Data</option>
            @endforelse
        </select>
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
