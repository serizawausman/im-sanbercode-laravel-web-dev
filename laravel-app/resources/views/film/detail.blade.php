@extends('layout.master')

@section('judul')
    <h1>Halaman Detail Film</h1>
@endsection

@section('content')
    
    <img src="{{ asset('image/' . $film->poster ) }}" class="card-img-top img-medium" alt="Poster of {{ $film->judul }}">
    <h5>{{ $film->judul }}</h5>
    <p class="card-text">{{ $film->ringkasan }}</p>
    <p class="card-text"><strong>Tahun:</strong> {{ $film->tahun }}</p>

    <hr>
    <h3>List Komentar</h3>
    @forelse ($film->ulasan as $item)
        <div class="media my-3 border p-3">
            <img src="https://dummyimage.com/600x400/000/fff" class="mr-3" style="border-radius: 60%" width="100px" alt="">
            <div class="media-body">
                <h5 class="mt-0">{{ $item->user->name }}</h5>
                <h5 class="mt-0">Rating: 
                    @for ($i = 0; $i < $item->rating; $i++)
                        &#9733;
                    @endfor
                    @for ($i = $item->rating; $i < 5; $i++)
                        &#9734;
                    @endfor
                </h5>
                <p>{{ $item->ulasan }}</p>
            </div>
        </div>
    @empty
        <h4>Belum ada komentar</h4>
    @endforelse
    <hr>
    <form action="/komentar/{{ $film->id }}" method="post">
    @csrf
    <textarea name="ulasan" class="form-control my-3" placeholder="Isi Komentar" rows="10"></textarea>
    @error('ulasan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror

    <div class="form-group">
        <label for="rating">Rating:</label>
        <select name="rating" class="form-control">
            <option value="">Pilih Rating</option>
            <option value="1">Sangat Buruk</option>
            <option value="2">Buruk</option>
            <option value="3">Cukup</option>
            <option value="4">Bagus</option>
            <option value="5">Sangat Bagus</option>
        </select>
    </div>
    @error('rating')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
    
    <input type="submit" value="Komentar" class="btn btn-primary">
    </form>
    <hr>
    <a href="/film" class="btn btn-info btn-block btn-sm">Kembali</a>

@endsection
