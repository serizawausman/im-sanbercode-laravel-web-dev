@extends('layout.master')

@section('judul')
    <h1>Dashboard</h1>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            @auth
                <h3>Selamat datang, {{ Auth::user()->name }}!</h3>
            @endauth

            @guest
                <h3>Selamat datang</h3>
            @endguest
        </div>
    </div>
</div>
@endsection
